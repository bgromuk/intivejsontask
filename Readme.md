# Intive Test Task (Gradle, Json, Java 8, HSQLDB)
Application that parse json file line by line, process that data and save result to HSQLDB.

To run simply write in terminal (in downloaded project folder):
```
$ ./gradlew run
```