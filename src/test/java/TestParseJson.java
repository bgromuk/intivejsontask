import beans.LogEntity;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class TestParseJson {
    public static final String TEST_JSON_PATH = "src/test/resources/error_sample.json";

    @Test
    public void testWithIncorrectErroLogs() {
        Path path = Paths.get(TEST_JSON_PATH);

        Map<String, LogEntity> stringLogEntityMap = ParseJson.parseJsonToEntities(path);
        List<LogEntity> filteredEntities = stringLogEntityMap.values().stream().filter(Objects::nonNull).collect(Collectors.toList());
        Assert.assertEquals(filteredEntities.size(), 3);

        Assert.assertTrue(stringLogEntityMap.containsKey("scsmbstgra"));
        Assert.assertTrue(stringLogEntityMap.get("scsmbstgra").getAlert());
        Assert.assertEquals(stringLogEntityMap.get("scsmbstgra").getType(), "APPLICATION_LOG");
        Assert.assertEquals(stringLogEntityMap.get("scsmbstgra").getHost(), "12345");

        Assert.assertTrue(stringLogEntityMap.containsKey("scsmbstgrb"));
        Assert.assertTrue(stringLogEntityMap.containsKey("scsmbstgrc"));
        Assert.assertTrue(stringLogEntityMap.get("scsmbstgrc").getAlert());

    }
}
