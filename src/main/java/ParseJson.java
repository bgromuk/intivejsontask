import beans.LogEntity;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParseJson {
    public static final String TEST_JSON_PATH = "src/main/resources/sample.json";
    public static final int DURATION_THRESHOLD = 4;
    public static final int START_FINISH_ACTIONS_CNT = 2;

    private static final Logger logger = LogManager.getLogger(ParseJson.class);
    public static final String TIMESTAMP = "timestamp";
    public static final String ID = "id";
    public static final String HOST = "host";
    public static final String TYPE = "type";
    public static final int START_EVT_INDEX = 0;
    public static final int FINISH_EVT_INDEX = 1;
    public static final String STATE = "state";
    public static final String STARTED = "STARTED";
    public static final String FINISHED = "FINISHED";


    public static void main(String args[]) {
        Configurator.setLevel("ParseJson", Level.DEBUG);

        Path path = Paths.get(TEST_JSON_PATH);
        Map<String, LogEntity> collect = parseJsonToEntities(path);

        List<LogEntity> entities = collect.values().stream().filter(Objects::nonNull).collect(Collectors.toList());

        truncateTable();
        insertToDB(entities);
        selctDataFromEvtTable();
    }

    static Map<String, LogEntity> parseJsonToEntities(Path path) {
        Map<String, LogEntity> collect = null;
        try {
            collect = Files.lines(path)
                    .map(parseAndValidateJsonRow())
                    .collect(Collectors.groupingBy(groupingByIdOfEvent(),
                            Collectors.collectingAndThen(
                                    Collectors.toList(), calculateDrationAndReturnEntity()
                            )));
        } catch (IOException e) {
            logger.error("Problems while reading json from file", e);
        }
        return collect;
    }

    private static Function<JSONObject, String> groupingByIdOfEvent() {
        return (JSONObject jobj) -> {
            if (jobj != null) {
                try {
                    return jobj.getString(ID);
                } catch (JSONException e) {
                    logger.error("Can't get id field from json row. String representation = {}", jobj, e);
                }
            }
            return "";
        };
    }

    private static Function<List<JSONObject>, LogEntity> calculateDrationAndReturnEntity() {
        return (List<JSONObject> logEntries) -> {
            // Need 2 entries - "from" and "to" to calculate duration
            if (logEntries.size() == START_FINISH_ACTIONS_CNT) {
                final JSONObject startEvent = logEntries.get(START_EVT_INDEX);
                final JSONObject endEvent = logEntries.get(FINISH_EVT_INDEX);
                try {
                    if (!startEvent.get(STATE).equals(endEvent.get(STATE))) {

                        final long startEvtTstmp = startEvent.getLong(TIMESTAMP);
                        final long endEvtTstmp = endEvent.getLong(TIMESTAMP);

                        final long duration = Math.abs(startEvtTstmp - endEvtTstmp);

                        final LogEntity logEntity = new LogEntity();
                        logEntity.setId(startEvent.getString(ID));
                        logEntity.setDuration(duration);
                        logEntity.setHost(startEvent.optString(HOST));
                        logEntity.setType(startEvent.optString(TYPE));

                        logEntity.setAlert(duration > DURATION_THRESHOLD);

                        return logEntity;
                    } else {
                        logger.info("States of events are equal");
                    }
                } catch (JSONException |NullPointerException e) {
                    logger.error("Error while retrieving fields from json rows to calculate duration, first row {}, second row {}.", e, startEvent, endEvent);
                }
            } else {
                logger.debug("Wrong number of events on duration calculation stage = {}.", logEntries.size());
            }
            return null;
        };
    }

    private static Function<String, JSONObject> parseAndValidateJsonRow() {
        return s -> {
            try {
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.has(ID) && jsonObject.has(TIMESTAMP) && jsonObject.has(STATE)) {
                    String state = jsonObject.getString(STATE);
                    if (state.contains(STARTED) || state.contains(FINISHED)) {
                        return jsonObject;
                    } else {
                        logger.info("Wrong state of event: {} ", state);
                    }
                } else {
                    logger.info("Insufficient of data in json -  'id' or'timestamp' or 'state' fields are missing. " +
                            "String representation: {}", s);
                }
            } catch (JSONException e) {
                logger.error("Can't parse json row", e);
            }
            return null;
        };
    }

    private static Connection connect() {
        String url = "jdbc:hsqldb:file:testdb";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, "SA", "");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    private static void insertToDB(List<LogEntity> entities) {
        String sql = "INSERT INTO \"PUBLIC\".\"EVENT_TABLE\"(id,duration,host,type,alert) VALUES(?,?,?,?,?)";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            for (LogEntity ent : entities) {
                pstmt.setString(1, ent.getId());
                pstmt.setDouble(2, ent.getDuration());
                pstmt.setString(3, ent.getHost());
                pstmt.setString(4, ent.getType());
                pstmt.setBoolean(5, ent.getAlert());
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error("Error while insertion to db", e);
        }
    }

    private static void selctDataFromEvtTable() {
        String sql = "SELECT * FROM \"PUBLIC\".\"EVENT_TABLE\"";

        try (Connection conn = connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                String id = rs.getString("id");
                Double duration = rs.getDouble("duration");
                String type = rs.getString("type");
                String host = rs.getString("host");
                Boolean alert = rs.getBoolean("alert");

                System.out.println("id : " + id);
                System.out.println("duration : " + duration);
                System.out.println("type : " + type);
                System.out.println("host : " + host);
                System.out.println("alert : " + alert);

                System.out.println("----------------------");

            }
        } catch (SQLException e) {
            logger.error("Error while insertion to db", e);
        }
    }

    private static void truncateTable() {
        try (Connection conn = connect();
             Statement pstmt = conn.createStatement();) {
            pstmt.executeUpdate("DELETE FROM \"PUBLIC\".\"EVENT_TABLE\"");
        } catch (SQLException e) {
            logger.error("Error while truncating event table", e);
        }
    }

}
